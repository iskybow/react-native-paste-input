import { Platform } from 'react-native';
export * from './types';
let PasteInput;

if (Platform.OS === 'android') {
  PasteInput = require('./android');
} else {
  PasteInput = require('./ios').default;
}

export default PasteInput;
//# sourceMappingURL=index.js.map