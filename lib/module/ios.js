function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React, { forwardRef, useEffect, useImperativeHandle, useMemo, useRef, useState } from 'react';
import { requireNativeComponent, TouchableWithoutFeedback } from 'react-native';
import TextInputState from 'react-native/Libraries/Components/TextInput/TextInputState';
import TextAncestor from 'react-native/Libraries/Text/TextAncestor';
const RCTPasteInput = requireNativeComponent('PasteInput');
const PasteInput = /*#__PURE__*/forwardRef((props, ref) => {
  let selection = useMemo(() => {
    var _props$selection$end;

    return !props.selection ? undefined : {
      start: props.selection.start,
      end: (_props$selection$end = props.selection.end) !== null && _props$selection$end !== void 0 ? _props$selection$end : props.selection.start
    };
  }, [props.selection]);
  const inputRef = useRef(null);
  const [mostRecentEventCount, setMostRecentEventCount] = useState(0);
  const [lastNativeText, setLastNativeText] = useState(props.value);
  const [lastNativeSelectionState, setLastNativeSelection] = useState({
    selection,
    mostRecentEventCount
  });
  const text = typeof props.value === 'string' ? props.value : typeof props.defaultValue === 'string' ? props.defaultValue : '';

  const viewCommands = require('react-native/Libraries/Components/TextInput/RCTMultilineTextInputNativeComponent').Commands;

  const lastNativeSelection = lastNativeSelectionState.selection;
  const lastNativeSelectionEventCount = lastNativeSelectionState.mostRecentEventCount;

  if (lastNativeSelectionEventCount < mostRecentEventCount) {
    selection = undefined;
  } // This is necessary in case native updates the text and JS decides
  // that the update should be ignored and we should stick with the value
  // that we have in JS.


  useEffect(() => {
    const nativeUpdate = {};

    if (lastNativeText !== props.value && typeof props.value === 'string') {
      nativeUpdate.text = props.value;
      setLastNativeText(props.value);
    }

    if (selection && lastNativeSelection && (lastNativeSelection.start !== selection.start || lastNativeSelection.end !== selection.end)) {
      nativeUpdate.selection = selection;
      setLastNativeSelection({
        selection,
        mostRecentEventCount
      });
    }

    if (Object.keys(nativeUpdate).length === 0) {
      return;
    }

    if (inputRef.current != null) {
      var _selection$start, _selection, _selection$end, _selection2;

      viewCommands.setTextAndSelection(inputRef.current, mostRecentEventCount, text, (_selection$start = (_selection = selection) === null || _selection === void 0 ? void 0 : _selection.start) !== null && _selection$start !== void 0 ? _selection$start : -1, (_selection$end = (_selection2 = selection) === null || _selection2 === void 0 ? void 0 : _selection2.end) !== null && _selection$end !== void 0 ? _selection$end : -1);
    }
  }, [mostRecentEventCount, inputRef, props.value, props.defaultValue, lastNativeText, selection, lastNativeSelection, text, viewCommands]);
  useEffect(() => {
    const inputRefValue = inputRef.current;

    if (inputRefValue != null) {
      TextInputState.registerInput(inputRefValue);
    }

    return () => {
      if (inputRefValue != null) {
        TextInputState.unregisterInput(inputRefValue);
      }
    };
  }, []);
  useEffect(() => {
    const inputRefValue = inputRef.current; // When unmounting we need to blur the input

    return () => {
      inputRefValue === null || inputRefValue === void 0 ? void 0 : inputRefValue.blur();
    };
  }, []);
  useImperativeHandle(ref, () => inputRef.current);

  const _onBlur = event => {
    TextInputState.blurInput(inputRef.current);

    if (props.onBlur) {
      props.onBlur(event);
    }
  };

  const _onChange = event => {
    const value = event.nativeEvent.text;
    props.onChange && props.onChange(event);
    props.onChangeText && props.onChangeText(value);

    if (inputRef.current == null) {
      // calling `props.onChange` or `props.onChangeText`
      // may clean up the input itself. Exits here.
      return;
    }

    setLastNativeText(value); // This must happen last, after we call setLastNativeText.
    // Different ordering can cause bugs when editing AndroidTextInputs
    // with multiple Fragments.
    // We must update this so that controlled input updates work.

    setMostRecentEventCount(event.nativeEvent.eventCount);
  };

  const _onFocus = event => {
    TextInputState.focusInput(inputRef.current);

    if (props.onFocus) {
      props.onFocus(event);
    }
  };

  const _onPaste = event => {
    if (props.onPaste) {
      const {
        data,
        error
      } = event.nativeEvent;
      props.onPaste(error === null || error === void 0 ? void 0 : error.message, data);
    }
  };

  const _onScroll = event => {
    props.onScroll && props.onScroll(event);
  };

  const _onSelectionChange = event => {
    props.onSelectionChange && props.onSelectionChange(event);

    if (inputRef.current == null) {
      // calling `props.onSelectionChange`
      // may clean up the input itself. Exits here.
      return;
    }

    setLastNativeSelection({
      selection: event.nativeEvent.selection,
      mostRecentEventCount
    });
  };

  return /*#__PURE__*/React.createElement(TextAncestor.Provider, {
    value: true
  }, /*#__PURE__*/React.createElement(TouchableWithoutFeedback, {
    onLayout: props.onLayout,
    onPressIn: props.onPressIn,
    onPressOut: props.onPressOut,
    accessible: props.accessible,
    accessibilityLabel: props.accessibilityLabel,
    accessibilityRole: props.accessibilityRole,
    accessibilityState: props.accessibilityState,
    testID: props.testID
  }, /*#__PURE__*/React.createElement(RCTPasteInput, _extends({}, props, {
    autoCapitalize: props.autoCapitalize || 'sentences',
    dataDetectorTypes: props.dataDetectorTypes,
    disableFullscreenUI: props.disableFullscreenUI,
    mostRecentEventCount: mostRecentEventCount,
    onBlur: _onBlur,
    onChange: _onChange,
    onContentSizeChange: props.onContentSizeChange,
    onFocus: _onFocus,
    onPaste: _onPaste,
    onScroll: _onScroll,
    onSelectionChange: _onSelectionChange,
    selection: selection,
    style: [props.style],
    textBreakStrategy: props.textBreakStrategy //@ts-ignore
    ,
    ref: inputRef
  }))));
});
export default PasteInput;
//# sourceMappingURL=ios.js.map