/// <reference types="react" />
import type { PastedFile } from '@iskybow/react-native-paste-input';
interface DetailsProps {
    file?: PastedFile;
}
declare const Details: ({ file }: DetailsProps) => JSX.Element | null;
export default Details;
