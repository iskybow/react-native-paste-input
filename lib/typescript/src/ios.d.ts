import React from 'react';
import type { PasteInputProps } from './types';
declare const PasteInput: React.ForwardRefExoticComponent<PasteInputProps & React.RefAttributes<unknown>>;
export default PasteInput;
