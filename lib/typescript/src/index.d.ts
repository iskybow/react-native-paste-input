import type React from 'react';
import type { PasteInputProps } from './types';
export * from './types';
declare let PasteInput: React.ForwardRefExoticComponent<PasteInputProps & React.RefAttributes<unknown>>;
export default PasteInput;
