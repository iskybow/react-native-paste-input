"use strict";

var React = _interopRequireWildcard(require("react"));

var _deprecatedReactNativePropTypes = require("deprecated-react-native-prop-types");

var _Platform = _interopRequireDefault(require("react-native/Libraries/Utilities/Platform"));

var _Text = _interopRequireDefault(require("react-native/Libraries/Text/Text"));

var _TextAncestor = _interopRequireDefault(require("react-native/Libraries/Text/TextAncestor"));

var _TextInputState = _interopRequireDefault(require("react-native/Libraries/Components/TextInput/TextInputState"));

var _invariant = _interopRequireDefault(require("invariant"));

var _nullthrows = _interopRequireDefault(require("nullthrows"));

var _setAndForwardRef = _interopRequireDefault(require("react-native/Libraries/Utilities/setAndForwardRef"));

var _usePressability = _interopRequireDefault(require("react-native/Libraries/Pressability/usePressability"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

const {
  useLayoutEffect,
  useRef,
  useState
} = React;
let AndroidTextInput;
let AndroidTextInputCommands;
AndroidTextInput = require('./android-native').default;
AndroidTextInputCommands = require('./android-native').Commands;

function InternalTextInput(props) {
  var _props$selection$end, _props$blurOnSubmit;

  const inputRef = useRef(null); // eslint-disable-next-line react-hooks/exhaustive-deps

  let selection = props.selection == null ? null : {
    start: props.selection.start,
    end: (_props$selection$end = props.selection.end) !== null && _props$selection$end !== void 0 ? _props$selection$end : props.selection.start
  };
  const [mostRecentEventCount, setMostRecentEventCount] = useState(0);
  const [lastNativeText, setLastNativeText] = useState(props.value);
  const [lastNativeSelectionState, setLastNativeSelection] = useState({
    selection,
    mostRecentEventCount
  });
  const lastNativeSelection = lastNativeSelectionState.selection;
  const lastNativeSelectionEventCount = lastNativeSelectionState.mostRecentEventCount;

  if (lastNativeSelectionEventCount < mostRecentEventCount) {
    selection = null;
  }

  const viewCommands = AndroidTextInputCommands;
  const text = typeof props.value === 'string' ? props.value : typeof props.defaultValue === 'string' ? props.defaultValue : ''; // This is necessary in case native updates the text and JS decides
  // that the update should be ignored and we should stick with the value
  // that we have in JS.

  useLayoutEffect(() => {
    const nativeUpdate = {};

    if (lastNativeText !== props.value && typeof props.value === 'string') {
      nativeUpdate.text = props.value;
      setLastNativeText(props.value);
    }

    if (selection && lastNativeSelection && (lastNativeSelection.start !== selection.start || lastNativeSelection.end !== selection.end)) {
      nativeUpdate.selection = selection;
      setLastNativeSelection({
        selection,
        mostRecentEventCount
      });
    }

    if (Object.keys(nativeUpdate).length === 0) {
      return;
    }

    if (inputRef.current != null) {
      var _selection$start, _selection, _selection$end, _selection2;

      viewCommands.setTextAndSelection(inputRef.current, mostRecentEventCount, text, (_selection$start = (_selection = selection) === null || _selection === void 0 ? void 0 : _selection.start) !== null && _selection$start !== void 0 ? _selection$start : -1, (_selection$end = (_selection2 = selection) === null || _selection2 === void 0 ? void 0 : _selection2.end) !== null && _selection$end !== void 0 ? _selection$end : -1);
    }
  }, [mostRecentEventCount, inputRef, props.value, props.defaultValue, lastNativeText, selection, lastNativeSelection, text, viewCommands]);
  useLayoutEffect(() => {
    const inputRefValue = inputRef.current;

    if (inputRefValue != null) {
      _TextInputState.default.registerInput(inputRefValue);

      return () => {
        _TextInputState.default.unregisterInput(inputRefValue);

        if (_TextInputState.default.currentlyFocusedInput() === inputRefValue) {
          (0, _nullthrows.default)(inputRefValue).blur();
        }
      };
    }
  }, [inputRef]);

  function clear() {
    if (inputRef.current != null) {
      viewCommands.setTextAndSelection(inputRef.current, mostRecentEventCount, '', 0, 0);
    }
  } // TODO: Fix this returning true on null === null, when no input is focused


  function isFocused() {
    return _TextInputState.default.currentlyFocusedInput() === inputRef.current;
  }

  function getNativeRef() {
    return inputRef.current;
  }

  const _setNativeRef = (0, _setAndForwardRef.default)({
    getForwardedRef: () => props.forwardedRef,
    setLocalRef: ref => {
      inputRef.current = ref;
      /*
      Hi reader from the future. I'm sorry for this.
      This is a hack. Ideally we would forwardRef to the underlying
      host component. However, since TextInput has it's own methods that can be
      called as well, if we used the standard forwardRef then these
      methods wouldn't be accessible and thus be a breaking change.
      We have a couple of options of how to handle this:
      - Return a new ref with everything we methods from both. This is problematic
      because we need React to also know it is a host component which requires
      internals of the class implementation of the ref.
      - Break the API and have some other way to call one set of the methods or
      the other. This is our long term approach as we want to eventually
      get the methods on host components off the ref. So instead of calling
      ref.measure() you might call ReactNative.measure(ref). This would hopefully
      let the ref for TextInput then have the methods like `.clear`. Or we do it
      the other way and make it TextInput.clear(textInputRef) which would be fine
      too. Either way though is a breaking change that is longer term.
      - Mutate this ref. :( Gross, but accomplishes what we need in the meantime
      before we can get to the long term breaking change.
      */

      if (ref) {
        ref.clear = clear;
        ref.isFocused = isFocused;
        ref.getNativeRef = getNativeRef;
      }
    }
  });

  const _onChange = event => {
    // eslint-disable-next-line no-shadow
    const text = event.nativeEvent.text;
    props.onChange && props.onChange(event);
    props.onChangeText && props.onChangeText(text);

    if (inputRef.current == null) {
      // calling `props.onChange` or `props.onChangeText`
      // may clean up the input itself. Exits here.
      return;
    }

    setLastNativeText(text); // This must happen last, after we call setLastNativeText.
    // Different ordering can cause bugs when editing AndroidTextInputs
    // with multiple Fragments.
    // We must update this so that controlled input updates work.

    setMostRecentEventCount(event.nativeEvent.eventCount);
  };

  const _onSelectionChange = event => {
    props.onSelectionChange && props.onSelectionChange(event);

    if (inputRef.current == null) {
      // calling `props.onSelectionChange`
      // may clean up the input itself. Exits here.
      return;
    }

    setLastNativeSelection({
      selection: event.nativeEvent.selection,
      mostRecentEventCount
    });
  };

  const _onFocus = event => {
    _TextInputState.default.focusInput(inputRef.current);

    if (props.onFocus) {
      props.onFocus(event);
    }
  };

  const _onBlur = event => {
    _TextInputState.default.blurInput(inputRef.current);

    if (props.onBlur) {
      props.onBlur(event);
    }
  };

  const _onScroll = event => {
    props.onScroll && props.onScroll(event);
  };

  const _onPaste = event => {
    if (props.onPaste) {
      const {
        data,
        error
      } = event.nativeEvent;
      props.onPaste(error === null || error === void 0 ? void 0 : error.message, data);
    }
  };

  let textInput = null; // The default value for `blurOnSubmit` is true for single-line fields and
  // false for multi-line fields.

  const blurOnSubmit = (_props$blurOnSubmit = props.blurOnSubmit) !== null && _props$blurOnSubmit !== void 0 ? _props$blurOnSubmit : !props.multiline;
  const accessible = props.accessible !== false;
  const focusable = props.focusable !== false;
  const config = React.useMemo(() => ({
    onPress: event => {
      if (props.editable !== false) {
        (0, _nullthrows.default)(inputRef.current).focus();
      }
    },
    onPressIn: props.onPressIn,
    onPressOut: props.onPressOut,
    cancelable: null
  }), // eslint-disable-next-line react-hooks/exhaustive-deps
  [props.editable, props.onPressIn, props.onPressOut, props.rejectResponderTermination]); // Hide caret during test runs due to a flashing caret
  // makes screenshot tests flakey

  let caretHidden = props.caretHidden;

  if (_Platform.default.isTesting) {
    caretHidden = true;
  } // TextInput handles onBlur and onFocus events
  // so omitting onBlur and onFocus pressability handlers here.


  const {
    onBlur,
    onFocus,
    ...eventHandlers
  } = (0, _usePressability.default)(config) || {};
  const style = [props.style];
  const autoCapitalize = props.autoCapitalize || 'sentences';
  let children = props.children;
  const childCount = React.Children.count(children);
  (0, _invariant.default)(!(props.value != null && childCount), 'Cannot specify both value and children.');

  if (childCount > 1) {
    children = /*#__PURE__*/React.createElement(_Text.default, null, children);
  }

  textInput =
  /*#__PURE__*/

  /* $FlowFixMe[prop-missing] the types for AndroidTextInput don't match up
   * exactly with the props for TextInput. This will need to get fixed */

  /* $FlowFixMe[incompatible-type] the types for AndroidTextInput don't
   * match up exactly with the props for TextInput. This will need to get
   * fixed */

  /* $FlowFixMe[incompatible-type-arg] the types for AndroidTextInput don't
   * match up exactly with the props for TextInput. This will need to get
   * fixed */
  React.createElement(AndroidTextInput, _extends({
    ref: _setNativeRef
  }, props, eventHandlers, {
    accessible: accessible,
    autoCapitalize: autoCapitalize,
    blurOnSubmit: blurOnSubmit,
    caretHidden: caretHidden,
    children: children,
    disableFullscreenUI: props.disableFullscreenUI,
    focusable: focusable,
    mostRecentEventCount: mostRecentEventCount,
    onBlur: _onBlur,
    onChange: _onChange,
    onFocus: _onFocus,
    onPaste: _onPaste
    /* $FlowFixMe[prop-missing] the types for AndroidTextInput don't match
     * up exactly with the props for TextInput. This will need to get fixed
     */

    /* $FlowFixMe[incompatible-type-arg] the types for AndroidTextInput
     * don't match up exactly with the props for TextInput. This will need
     * to get fixed */
    ,
    onScroll: _onScroll,
    onSelectionChange: _onSelectionChange,
    selection: selection,
    style: style,
    text: text,
    textBreakStrategy: props.textBreakStrategy
  }));
  return /*#__PURE__*/React.createElement(_TextAncestor.default.Provider, {
    value: true
  }, textInput);
}

const ExportedForwardRef = /*#__PURE__*/React.forwardRef(function TextInput({
  allowFontScaling = true,
  rejectResponderTermination = true,
  underlineColorAndroid = 'transparent',
  ...restProps
}, forwardedRef) {
  return /*#__PURE__*/React.createElement(InternalTextInput, _extends({
    allowFontScaling: allowFontScaling,
    rejectResponderTermination: rejectResponderTermination,
    underlineColorAndroid: underlineColorAndroid
  }, restProps, {
    forwardedRef: forwardedRef
  }));
}); // TODO: Deprecate this

ExportedForwardRef.propTypes = _deprecatedReactNativePropTypes.TextPropTypes; // $FlowFixMe[prop-missing]

ExportedForwardRef.State = {
  currentlyFocusedInput: _TextInputState.default.currentlyFocusedInput,
  currentlyFocusedField: _TextInputState.default.currentlyFocusedField,
  focusTextInput: _TextInputState.default.focusTextInput,
  blurTextInput: _TextInputState.default.blurTextInput
}; // $FlowFixMe[unclear-type] Unclear type. Using `any` type is not safe.

module.exports = ExportedForwardRef;
//# sourceMappingURL=android.js.map