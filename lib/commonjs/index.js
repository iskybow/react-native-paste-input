"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var _exportNames = {};
exports.default = void 0;

var _reactNative = require("react-native");

var _types = require("./types");

Object.keys(_types).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _types[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _types[key];
    }
  });
});
let PasteInput;

if (_reactNative.Platform.OS === 'android') {
  PasteInput = require('./android');
} else {
  PasteInput = require('./ios').default;
}

var _default = PasteInput;
exports.default = _default;
//# sourceMappingURL=index.js.map